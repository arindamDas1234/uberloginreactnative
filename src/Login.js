import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, ImageBackground, Image, TextInput, Animated, Dimensions } from 'react-native';

import * as Animatable from 'react-native-animatable';

const Screen_height = Dimensions.get('window').height;


class Login extends Component {

	componentWillMount(){
	this.loginHeight= new Animated.Value(150);
}

increseHeight = ()=>{
	Animated.timing(this.loginHeight,{
		toValue:Screen_height,
		duration:500,
		useNativeDriver:false

	}).start()
}

decreaseHeight = ()=>{
	Animated.timing(this.loginHeight,{
		toValue:150,
		duration:500,
		useNativeDriver:false
	}).start()
}


	render(){

	const headerTitleOpacity = this.loginHeight.interpolate({
		inputRange:[150,Screen_height],
		outputRange:[1, 0]
	});

	const marginTopOpacity = this.loginHeight.interpolate({
		inputRange:[150, Screen_height],
		outputRange:[15,50]
	})

	const headerBackArrowOpacity = this.loginHeight.interpolate({
		inputRange:[150, Screen_height],
		outputRange:[0,1]
	})
		return(

			<View style={{ flex:1 }} >

		<ImageBackground 

			source={{uri:'https://image.freepik.com/free-psd/bio-food-flyer-template-with-photo_23-2148211557.jpg'}}

			style={{
				width:'100%',
				height:'100%'
			}}
		 >
<Animatable.View animation = 'zoomIn' iterationCount={1} style={{ justifyContent:'center',alignItems:'center',  flex:1 }} >

		 <Text style={{  fontSize:30, fontWeight:'bold',height:120, width:120, backgroundColor:'#fff', textAlign:'center', padding:23 }} > Uber </Text>
		  </ Animatable.View>
<Animatable.View animation = 'slideInUp' iterationCount={1} >


		<Animated.View style={{
			
			backgroundColor:'#fff',
			height:this.loginHeight
		}} >

		<Animated.View style={{
			height:20,
			width:20,
			position:'absolute',
			left:20,
			top:20,
			zindex:100,
			opacity:headerBackArrowOpacity
		}} >

		<TouchableOpacity onPress={()=> this.decreaseHeight() } >

	<Image source={{ uri:'https://www.pngkit.com/png/detail/191-1910884_left-back-arrow-outline-vector-white-back-arrow.png' }} 

		style={{
			height:20,
			width:20
		}}
	 />
	</TouchableOpacity>


		</Animated.View>

  <Animated.View style={{
  	fontSize:20,
   	 fontWeight:'bold',
   	 alignItems:'flex-start',
   	 marginTop:marginTopOpacity,
   	 paddingHorizontal:25,
   	 opacity:headerTitleOpacity
  }} >



 <Text style={{
 	fontSize:24
 }} > Get Start With Uber </Text>

  </Animated.View>

<TouchableOpacity onPress={()=> this.increseHeight() } >

  <Animated.View pointerEvents="none" style={{  flexDirection:'row', justifyContent:'center', alignItems:'center', paddingHorizontal:30, marginTop:10, opacity:1 }} >

   		<Image source={{ uri:'https://image.shutterstock.com/image-vector/india-flag-official-colors-proportion-260nw-425893018.jpg' }}

   			style={{
   				height:24,
   				width:24,
   				padding:10,
   				margin:10
   			}}
   		  />

   		  <Text style={{
   		  	 fontSize:15,
   		  	 
   		  }} >+91</Text>

   		  <TextInput placeholder="Enter Your  Number" style={{
   		  	flex:1,
   		  
   
   		  }} />
   </ Animated.View>

</TouchableOpacity>
		</Animated.View>

</Animatable.View>

		  <View style={{
		  	 backgroundColor:'#fff',
		  	 height:70,
		  	 alignItems:'flex-start',
		  	 justifyContent:'center',
		  	 paddingHorizontal:30

		  }} >

	<Text style={{ fontSize:14, color:'#5a7fdf', textAlign:'center' }} > Login Your Account With Social Account </Text>

		  </View>

	</ImageBackground>
			</View>
		)
	}
}

export default Login;